'use strict';

const Service = require('egg').Service;

class PostService extends Service {

  async create(post) {
    const { ctx } = this;

    await ctx.model.post.create(post);

    return true;
  }

  async getPostBySlug(postSlug) {
    const { ctx } = this;

    const post = await ctx.model.post.findOne({
      postSlug,
    });

    return post;
  }
}

module.exports = PostService;
