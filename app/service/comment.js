'use strict';

const Service = require('egg').Service;

class CommentService extends Service {
  /**
   * @param {string} post comment slug.
   * @return {boolean} is create success.
   */
  async create(post) {
    const { ctx } = this;

    await ctx.model.post.create(post);

    return true;
  }

  /**
   * Adds two numbers together.
   * @param {int} postSlug first number.
   * @return {int} The sum of the two numbers.
   */
  async getPostBySlug(postSlug) {
    const { ctx } = this;

    const post = await ctx.model.post.findOne({
      postSlug,
    });

    return post;
  }
}

module.exports = CommentService;
