'use strict';

const Service = require('egg').Service;

class UserService extends Service {

  async loginAndGetUser(identity, password) {
    const { ctx } = this;

    const user = await ctx.model.User.findOne({
      where: {
        username: identity,
        password,
      },
    });

    return user;
  }

  async createUser({
    username,
    password,
    email,
  }) {
    const { ctx } = this;

    const user = await ctx.model.User.findOne({
      where: {
        username,
      },
    });

    if (user) {
      return null;
    }

    return await ctx.model.User.create({
      username,
      password,
      email,
    });
  }

  async getUserByUsername(username) {
    const { ctx } = this;

    const user = await ctx.model.user.findOne({
      username,
    });

    return user;
  }
}

module.exports = UserService;
