'use strict';

/**
 * @param {Egg.Application} app - egg application
 */
module.exports = app => {
  const { router, controller } = app;
  router.get('/', controller.page.index);
  router.get('/login', controller.page.login);
  router.get('/register', controller.page.register);

  router.get('/api/user/:username', controller.api.user.index);
  router.post('/api/user/login', controller.api.user.login);
  router.post('/api/user/register', controller.api.user.register);
  router.get('/api/post/:post_id', controller.api.post.index);
  router.post('/api/post', controller.api.post.create);
};
