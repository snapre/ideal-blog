'use strict';

const Controller = require('egg').Controller;

class HomeController extends Controller {
  get commonPageConfig() {
    const { app } = this;
    return {
      env: app.config.env,
      version: app.config.pkg.version,
    };
  }

  async index() {
    const { ctx, app } = this;

    ctx.body = await app.render(
      {},
      {
        ...this.commonPageConfig,
        title: '首页',
        pageId: 'home',
      }
    );
  }

  async login() {
    const { ctx, app } = this;

    ctx.body = await app.render(
      {},
      {
        ...this.commonPageConfig,
        title: '登录',
        pageId: 'login',
      }
    );
  }

  async register() {
    const { ctx, app } = this;

    ctx.body = await app.render(
      {},
      {
        ...this.commonPageConfig,
        title: '注册',
        pageId: 'register',
      }
    );
  }
}

module.exports = HomeController;
