'use strict';

const Controller = require('egg').Controller;
const ms = require('ms');

class UserController extends Controller {
  /**
   * get user info by username.
   */
  async index() {
    const { ctx } = this;

    const user = await ctx.service.user.findOne({
      username: ctx.params.username,
    });

    ctx.success({
      user,
    });
  }

  /**
   * create a new user
   */
  async register() {
    const { ctx } = this;

    const { email, username, password } = ctx.request.body;

    const user = await ctx.service.user.createUser({
      username,
      password,
      email,
    });

    ctx.body = {
      success: !!user,
      user,
    };
  }

  async login() {
    const { ctx } = this;

    const { identity, password, rememberMe } = ctx.request.body;

    console.log(ctx.request.body);

    const user = await ctx.service.user.loginAndGetUser(identity, password);

    // 设置 Session
    // ctx.session.user = user;
    // 如果用户勾选了 `记住我`，设置 30 天的过期时间
    // if (rememberMe) ctx.session.maxAge = ms('30d');

    ctx.body = {
      success: !!user,
      user,
    };
  }
}

module.exports = UserController;
