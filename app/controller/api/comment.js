'use strict';

const Controller = require('egg').Controller;

class CommentController extends Controller {
  /**
   * get user info by username.
   */
  async index() {
    const { ctx } = this;
    const user = await ctx.service.user.findOne({
      username: ctx.params.username,
    });

    ctx.success({
      user,
    });
  }

  /**
   * create a new user.
   */
  async create() {
    const { ctx } = this;
    const params = ctx.params;
    await ctx.service.user.create(params);

    ctx.success({
      result: true,
    });
  }
}

module.exports = CommentController;
