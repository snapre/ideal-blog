'use strict';

module.exports = app => {
  const {
    STRING,
    UUID,
    UUIDV4,
  } = app.Sequelize;

  return app.model.define('post', {
    uniqId: {
      type: UUID,
      defaultValue: UUIDV4,
      primaryKey: true,
      allowNull: false,
      field: 'uniq_id',
    },
    title: {
      type: STRING,
      allowNull: false,
      field: 'title',
    },
    content: {
      type: STRING,
      allowNull: false,
      field: 'content',
    },
    authorUniqId: {
      type: STRING,
      allowNull: true,
      field: 'author_id',
    },
  }, {
    ...app.config.modelCommonOption,
    indexes: [
      {
        fields: [
          'uniqId',
        ],
        unique: true,
      },
    ],
  });
};
