'use strict';

module.exports = app => {
  const {
    STRING,
    DATE,
    UUID,
    UUIDV4,
  } = app.Sequelize;

  const User = app.model.define('user', {
    uniqId: {
      type: UUID,
      defaultValue: UUIDV4,
      primaryKey: true,
      allowNull: false,
      field: 'uniq_id',
    },
    username: {
      type: STRING,
      allowNull: false,
      field: 'username',
    },
    password: {
      type: STRING,
      allowNull: false,
      field: 'password',
    },
    name: {
      type: STRING,
      allowNull: true,
      field: 'name',
    },
    gender: {
      type: STRING,
      allowNull: true,
      field: 'gender',
    },
    description: {
      type: STRING,
      allowNull: true,
      field: 'description',
    },
    avatar: {
      type: STRING,
      allowNull: true,
      field: 'avatar',
    },
    mobile: {
      type: STRING,
      allowNull: true,
      field: 'mobile',
    },
    email: {
      type: STRING,
      allowNull: false,
      field: 'email',
    },
    createdAt: {
      type: DATE,
      allowNull: false,
      field: 'created_at',
    },
    updatedAt: {
      type: DATE,
      allowNull: false,
      field: 'updated_at',
    },
  }, {
    ...app.config.modelCommonOption,
    indexes: [
      {
        fields: [
          'username',
        ],
        unique: true,
      },
      {
        fields: [
          'uniq_id',
        ],
        unique: true,
      },
    ],
  });

  return User;
};
