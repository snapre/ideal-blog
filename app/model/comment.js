'use strict';

module.exports = app => {
  const {
    STRING,
    UUID,
    UUIDV4,
    INTEGER,
  } = app.Sequelize;

  return app.model.define('comment', {
    uniqId: {
      type: UUID,
      defaultValue: UUIDV4,
      primaryKey: true,
      allowNull: false,
      field: 'uniq_id',
    },
    authorId: {
      type: INTEGER,
      allowNull: false,
      field: 'author_id',
    },
    content: {
      type: STRING,
      allowNull: false,
      field: 'content',
    },
    postId: {
      type: INTEGER,
      allowNull: false,
      field: 'post_id',
    },
  }, {
    ...app.config.modelCommonOption,
    indexes: [
      {
        fields: [
          'uniqId',
        ],
        unique: true,
      },
    ],
  });
};
