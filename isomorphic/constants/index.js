'use strict';

const doc = require('./doc');
const event = require('./event');

module.exports = {
  doc,
  event,
};
