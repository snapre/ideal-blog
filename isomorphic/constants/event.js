'use strict';

module.exports = {
  IPC_EVENTS: {
    COMMON: {
      OPEN_ELECTROM: 'common/open-electrom',
      GET_GLOBAL_CONFIG: 'common/get-global-config',
    },
  },
};
