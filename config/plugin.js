'use strict';

/** @type Egg.EggPlugin */

exports.static = {
  enable: true,
};

exports.cors = {
  enable: true,
  package: 'egg-cors',
};

exports.sequelize = {
  enable: true,
  package: 'egg-sequelize',
};

exports.sessionRedis = {
  enable: true,
  package: 'egg-session-redis',
};

exports.redis = {
  enable: true,
  package: 'egg-redis',
};

exports.validate = {
  enable: true,
  package: 'egg-validate',
};
