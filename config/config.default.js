/* eslint valid-jsdoc: "off" */

'use strict';

const dbConfig = require('../database/config');
const path = require('path');

/**
 * @param {Egg.EggAppInfo} appInfo app info
 */
module.exports = appInfo => {
  /**
   * built-in config
   * @type {Egg.EggAppConfig}
   **/
  const config = exports = {};

  // use for cookie sign key, should change to your own and keep security
  config.keys = appInfo.name + '_1636851066731_3336';

  // add your middleware config here
  config.middleware = [ 'gzip' ];

  // 配置 gzip 中间件的配置
  config.gzip = {
    threshold: 1024, // 小于 1k 的响应体不压缩
  };

  // add your user config here
  const userConfig = {
    myAppName: 'egg-blog',
  };

  config.security = {
    xframe: false,
    csrf: {
      enable: false,
    },
    methodnoallow: {
      enable: false,
    },
  };

  config.cors = {
    origin: '*',

    // credentials: true,
    allowMethods: 'GET,HEAD,PUT,POST,DELETE,PATCH',
  };

  config.sequelize = dbConfig.development;

  config.static = {
    prefix: '',
    dir: path.resolve(__dirname, '..', 'view'),
  };

  return {
    ...config,
    ...userConfig,
  };
};
