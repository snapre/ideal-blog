import { defineConfig } from 'vite';
import react from '@vitejs/plugin-react';
import path from 'path';

export default defineConfig({
  build: {
    manifest: true,
    rollupOptions: {
      input: path.join(__dirname, 'src', 'main.jsx'),
    }
  },
  // esbuild: {
  //   jsxInject: `import React from 'react'`,
  // },
  css: {
    modules: {
      generateScopedName: '[name]__[local]___[hash:base64:5]',
      hashPrefix: 'prefix',
    },
    preprocessorOptions: {
      less: {
        javascriptEnabled: true,
      },
    },
  },
  plugins: [
    react(),
  ]
})
