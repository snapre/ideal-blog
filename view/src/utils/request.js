import axios from "axios";

const service = axios.create({
  baseURL: 'http://127.0.0.1:7001',
  timeout: 1000,
  credentials: 'same-origin',
  headers: {
  'X-Custom-Header': 'foobar',
}
});

// request interceptor
service.interceptors.request.use(
  config => {
    config.headers['Content-Type'] = 'application/json; charset=utf-8';
    config.headers['Accept'] = '*/*';
    return config
  },
  error => {
    console.error(error) // for debug
    return Promise.reject(error)
  }
)

export default service;
