import { useState } from "react";
import { Routes, Route, Link } from "react-router-dom";
import logo from "./assets/icons/favicon.svg";
import "./App.css";
import Login from "./pages/Login";
import Register from "./pages/Register";
import PostList from "./components/PostList";
import { Layout, Menu, Breadcrumb, Button, Divider } from "antd";
import CommentList from "./components/comment/CommentList";
import CommentEditor from "./components/comment/CommentEditor";
import Icon from "@ant-design/icons";

const { Header, Content, Footer } = Layout;

function App() {
  const [count, setCount] = useState(0);

  return (
    <div className="App">
      <Routes>
        <Route path="/" element={<Home />} />
        <Route path="/about" element={<About />} />
        <Route path="/login" element={<Login />} />
        <Route path="/register" element={<Register />} />
      </Routes>
    </div>
  );
}

function Home() {
  return (
    <Layout>
      <Header style={{ position: "fixed", zIndex: 1, width: "100%" }}>
        <a className="logo" href="/">
          <Icon
            style={{ fontSize: 32, color: '#fff' }}
            component={() => <img width={64} src={logo} alt="logo" />}
          />
          {/* <span className="logo-text">Snapre Group</span> */}
        </a>
        {/* <div className="menu">
          <Menu theme="dark" mode="horizontal" defaultSelectedKeys={["2"]}>
            <Menu.Item key="1">nav 1</Menu.Item>
            <Menu.Item key="2">nav 2</Menu.Item>
            <Menu.Item key="3">nav 3</Menu.Item>
          </Menu>
        </div> */}
        <div className="login">
          <Button type="primary" href="/login">
            Login
          </Button>{" "}
          &nbsp;&nbsp;
          <Button type="primary" href="/register">
            Register
          </Button>
        </div>
      </Header>
      <Content
        className="site-layout"
        style={{ padding: "0 50px", marginTop: 64 }}
      >
        <Breadcrumb style={{ margin: "16px 0" }}>
          <Breadcrumb.Item>Home</Breadcrumb.Item>
          <Breadcrumb.Item>List</Breadcrumb.Item>
          <Breadcrumb.Item>App</Breadcrumb.Item>
        </Breadcrumb>
        <div
          className="site-layout-background"
          style={{ padding: 24, minHeight: 380 }}
        >
          <PostList />
          <Divider />
          <CommentList />
          <Divider />
          <CommentEditor />
        </div>
      </Content>
      <Footer style={{ textAlign: "center" }}>
        Ideal Blog ©2022 Created by Snapre Group
      </Footer>
    </Layout>
  );
}

function About() {
  return (
    <>
      <main>
        <h2>Who are we?</h2>
        <p>That feels like an existential question, don't you think?</p>
      </main>
      <nav>
        <Link to="/">Home</Link> &nbsp;
        <Link to="/login">Login</Link> &nbsp;
        <Link to="/register">Register</Link>
      </nav>
    </>
  );
}

export default App;
