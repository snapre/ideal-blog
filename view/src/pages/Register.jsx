import React from "react";
import styles from "./Register.module.less";
import RegisterForm from "../components/forms/RegisterForm";

import { Button, notification } from 'antd';

const openNotification = (value) => {
  notification.open({
    message: value.success ? 'Register Success !' : 'Register Fail !',
    description: JSON.stringify(value),
    onClick: () => {
      console.log('Notification Clicked!');
    },
  });
};


import { userApi } from "../api";

function Register() {
  return (
    <div className={styles.register}>
      <h1>Register Page</h1>
      <RegisterForm
        onOk={async (values) => {
          const res = await userApi.registerApi(values);
          openNotification(res.data);
          console.log(res);
        }}
      />
    </div>
  );
}

export default Register;
