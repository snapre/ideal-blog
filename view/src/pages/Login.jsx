import React from "react";
import { Button, notification } from 'antd';
import styles from "./Login.module.less";
import LoginForm from "../components/forms/LoginForm";

const openNotification = (value) => {
  notification.open({
    message: value.success ? 'Login Success !' : 'Login Fail !',
    description: JSON.stringify(value),
    onClick: () => {
      console.log('Notification Clicked!');
    },
  });
};

import { userApi } from "../api";

function Login() {
  return (
    <div className={styles.login}>
      <div>
        <h1>Login Page</h1>
      </div>
      <LoginForm
        onOk={async (values) => {
          const res = await userApi.loginApi(values);
          openNotification(res.data);
          console.log(res);
        }}
      />
    </div>
  );
}

export default Login;
