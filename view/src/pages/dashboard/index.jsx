import React from "react";

function Dashboard() {
  return (
    <>
      <main>
        <h2>Welcome to the homepage!</h2>
        <p>You can do this, I believe in you.</p>
      </main>
      <nav>
        <Link to="/about">About</Link> &nbsp;
        <Link to="/login">Login</Link> &nbsp;
        <Link to="/register">Register</Link>
      </nav>
    </>
  );
}

export default Dashboard;