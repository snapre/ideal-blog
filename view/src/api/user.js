import request from '../utils/request';

/**
 * user login api
 * @param {string} identity 用户唯一标识（用户名、邮箱或手机号）
 * @param {string} password 用户密码（MD5加密）
 */
export function loginApi(data) {
  return request({
    url: '/api/user/login',
    method: 'post',
    data,
  });
}

/**
 * user register api
 */
 export function registerApi(data) {
  return request({
    url: '/api/user/register',
    method: 'post',
    data,
  })
}

/**
 * 获取所有用户信息接口
 */
export function getAllUsersApi() {
  return request({
    url: '/api/users',
    method: 'get',
  })
}
