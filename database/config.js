'use strict';

const defaultConfig = {
  username: 'root',
  database: 'ideal_blog_dev',
  host: process.env.MYSQL_HOST || '127.0.0.1',
  port: process.env.MYSQL_PORT || '3306',
  operatorsAliases: 0,
  dialect: 'mysql',
  define: {
    underscored: false,
  },
};

module.exports = {
  development: defaultConfig,
  test: Object.assign({}, defaultConfig, {
    database: 'ideal_blog_unittest',
  }),
  production: Object.assign({}, defaultConfig, {
    defaultConfig,
    database: 'ideal_blog',
  }),
};
