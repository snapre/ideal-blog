'use strict';

module.exports = {
  async up(queryInterface) {
    await queryInterface.addIndex('users', [ 'username' ], { unique: true });
  },

  async down(queryInterface) {
    await queryInterface.removeIndex('users', [ 'username' ], { unique: true });
  },
};
