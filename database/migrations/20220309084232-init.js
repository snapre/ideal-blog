'use strict';

module.exports = {
  up: async (queryInterface, Sequelize) => {
    const {
      STRING,
      INTEGER,
      UUID,
      UUIDV4,
      DATE,
    } = Sequelize;

    await queryInterface.createTable('users', {
      uniqId: {
        type: UUID,
        defaultValue: UUIDV4,
        primaryKey: true,
        allowNull: false,
        field: 'uniq_id',
      },
      username: {
        type: STRING,
        allowNull: false,
        field: 'username',
      },
      password: {
        type: STRING,
        allowNull: false,
        field: 'password',
      },
      name: {
        type: STRING,
        allowNull: true,
        field: 'name',
      },
      gender: {
        type: STRING,
        allowNull: true,
        field: 'gender',
      },
      description: {
        type: STRING,
        allowNull: true,
        field: 'description',
      },
      avatar: {
        type: STRING,
        allowNull: true,
        field: 'avatar',
      },
      mobile: {
        type: STRING,
        allowNull: true,
        field: 'mobile',
      },
      email: {
        type: STRING,
        allowNull: false,
        field: 'email',
      },
      createdAt: {
        type: DATE,
        allowNull: false,
        field: 'created_at',
      },
      updatedAt: {
        type: DATE,
        allowNull: false,
        field: 'updated_at',
      },
    }, {
      indexes: [
        {
          fields: [
            'username',
          ],
          unique: true,
        },
        {
          fields: [
            'uniq_id',
          ],
          unique: true,
        },
      ],
    });

    await queryInterface.createTable('posts', {
      uniqId: {
        type: UUID,
        defaultValue: UUIDV4,
        primaryKey: true,
        allowNull: false,
        field: 'uniq_id',
      },
      title: {
        type: STRING,
        allowNull: false,
        field: 'title',
      },
      content: {
        type: STRING,
        allowNull: false,
        field: 'content',
      },
      authorId: {
        type: STRING,
        allowNull: true,
        field: 'author_id',
      },
      pv: {
        type: INTEGER,
        default: 0,
        field: 'pv',
      },
      createdAt: {
        type: DATE,
        allowNull: false,
        field: 'created_at',
      },
      updatedAt: {
        type: DATE,
        allowNull: false,
        field: 'updated_at',
      },
    }, {
      indexes: [
        {
          fields: [
            'uniqId',
          ],
          unique: true,
        },
      ],
    });

    await queryInterface.createTable('comments', {
      uniqId: {
        type: UUID,
        defaultValue: UUIDV4,
        primaryKey: true,
        allowNull: false,
        field: 'uniq_id',
      },
      authorId: {
        type: INTEGER,
        allowNull: false,
        field: 'author_id',
      },
      content: {
        type: STRING,
        allowNull: false,
        field: 'content',
      },
      postId: {
        type: INTEGER,
        allowNull: false,
        field: 'post_id',
      },
      created_at: {
        type: DATE,
        allowNull: false,
      },
      updated_at: {
        type: DATE,
        allowNull: false,
      },
    }, {
      indexes: [
        {
          fields: [
            'uniqId',
          ],
          unique: true,
        },
      ],
    });
  },

  down: async queryInterface => {
    await queryInterface.dropTable('users');
    await queryInterface.dropTable('posts');
    await queryInterface.dropTable('comments');
  },
};
